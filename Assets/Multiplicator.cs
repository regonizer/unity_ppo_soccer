using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Multiplicator : MonoBehaviour
{
  public GameObject prefab;
  public int amout = 0;
  public float offset = 20f;

  void Start() {
    foreach (int index in Enumerable.Range( 1, amout )) {
      GameObject go = Instantiate(prefab, Vector3.zero + Vector3.forward * index * offset, new Quaternion());
      go.GetComponent<GameMaster>().id = index;
    }
  }
}
