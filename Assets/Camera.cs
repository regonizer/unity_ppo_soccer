using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
  [Range(0, 20)]
  public float lerp_speed = 1f;
  public Transform position;

  void Start() {
    transform.position = position.position;
    transform.rotation = position.rotation;
  }

  void Update() {
    transform.position = Vector3.Lerp(transform.position, position.position, lerp_speed * Time.deltaTime);
    Vector3 rotation = transform.rotation.eulerAngles;
    Vector3 new_rotation = Quaternion.Lerp(transform.rotation, position.rotation, lerp_speed * Time.deltaTime).eulerAngles;
    transform.rotation = Quaternion.Euler(new_rotation.x, new_rotation.y, rotation.z);
  }
}
