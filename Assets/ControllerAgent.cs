using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Policies;

public class ControllerAgent : Agent
{
  [Range(1,100)]
  public float force_move = 5f;
  [Range(1,1000)]
  public float force_rotation = 10f;
  [HideInInspector]
  public float past_distance_to_ball = 10f;
  public GameObject ball;
  public GameObject goal;
  public GameObject owngoal;
  [HideInInspector]
  public Vector3 pos_agent;
  [HideInInspector]
  public Quaternion rot_agent;
  public GameObject enemy;
  private Rigidbody ball_rigidbody;
  private Rigidbody rigidbody;
  private float move = 0f;
  private float rotate = 0f;
  private bool[] move_action = new bool[3];
  private bool[] rotate_action = new bool[3];
  private GameMaster game_master;
  private ControllerHuman controller_human;

  void Start() {
    rigidbody = GetComponent<Rigidbody>();
    ball_rigidbody = ball.GetComponent<Rigidbody>();
    pos_agent = transform.position;
    rot_agent = transform.rotation;
    //transform.RotateAround(transform.position, transform.up, Random.Range(0f, 360f));
    game_master = GetComponentInParent<GameMaster>();
    controller_human = GetComponent<ControllerHuman>();
  }

  void OnCollisionEnter(Collision collision)
  {
    // give huge reward for hitten the ball
    if (ball != collision.gameObject) return;
    AddReward(0.01f);
  }

  void FixedUpdate() {
    // apply decision to physical agent 
    if (controller_human.enabled) return;
    float backward_factor = (move < 0f) ? 0.8f : 1f; // moving full speed backwards is not allowed
    rigidbody.AddForce(backward_factor * move * transform.forward * force_move);
    transform.RotateAround(transform.position, transform.up, rotate * force_rotation * Time.deltaTime);
  }

  public override void OnEpisodeBegin() {
  }

  public override void CollectObservations(VectorSensor sensor) {
    // ball position
    sensor.AddObservation(ball.transform.localPosition.x); // -16 to 16
    sensor.AddObservation(ball.transform.localPosition.z); // -10 to 10
    sensor.AddObservation(ball_rigidbody.velocity.x);
    sensor.AddObservation(ball_rigidbody.velocity.y);
    // position 
    sensor.AddObservation(transform.localPosition.x); // -16 to 16
    sensor.AddObservation(transform.localPosition.z); // -10 to 10
    sensor.AddObservation(rigidbody.velocity.x);
    sensor.AddObservation(rigidbody.velocity.y);
    sensor.AddObservation(transform.localRotation.eulerAngles.y);
    sensor.AddObservation(transform.localRotation.eulerAngles.y - 180);
    // relative position to ball
    sensor.AddObservation((ball.transform.localPosition - transform.localPosition).x); // -16 to 16
    sensor.AddObservation((ball.transform.localPosition - transform.localPosition).z); // -10 to 10
    // ball target position (enemy goal)
    sensor.AddObservation(goal.transform.localPosition.x); // static for reference
    sensor.AddObservation(goal.transform.localPosition.z); // static for reference
    // ball negativ target position (own goal)
    sensor.AddObservation(owngoal.transform.localPosition.x); // static for reference
    sensor.AddObservation(owngoal.transform.localPosition.z); // static for reference
    // enemy
    sensor.AddObservation(enemy.transform.localPosition.x); // -16 to 16
    sensor.AddObservation(enemy.transform.localPosition.z); // -10 to 10
  }

  public override void Heuristic(in ActionBuffers actionsOut)
  {
    //var discreteActionsOut = actionsOut.DiscreteActions;
    //discreteActionsOut[0] = 1;
    //discreteActionsOut[1] = 1;
    //if (Input.GetKey(KeyCode.W)) discreteActionsOut[0] += 1;
    //if (Input.GetKey(KeyCode.S)) discreteActionsOut[0] -= 1;
    //if (Input.GetKey(KeyCode.D)) discreteActionsOut[1] += 1;
    //if (Input.GetKey(KeyCode.A)) discreteActionsOut[1] -= 1;
  }

  public override void OnActionReceived(ActionBuffers actions) {
    move = actions.DiscreteActions[0] - 1;
    rotate = actions.DiscreteActions[1] - 1;
  }
}
