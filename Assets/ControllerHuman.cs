using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerHuman : MonoBehaviour
{
  [Range(1,100)]
  public float force_move = 5f;
  [Range(1,1000)]
  public float force_rotation = 3f;

  private float move = 0f;
  private float rotate = 0f;
  private Rigidbody rbody;

  void Start() {
    rbody = GetComponent<Rigidbody>();
  }

  void FixedUpdate() {
    move = 0f;
    rotate = 0f;
    if (Input.GetKey(KeyCode.W)) move += 1f;
    if (Input.GetKey(KeyCode.S)) move -= 1f;
    if (Input.GetKey(KeyCode.D)) rotate += 1f;
    if (Input.GetKey(KeyCode.A)) rotate -= 1f;

    float backward_factor = (move < 0f) ? 0.8f : 1f; // moving full speed backwards is not allowed
    rbody.AddForce(backward_factor * move * force_move * transform.forward);
    transform.RotateAround(transform.position, transform.up, rotate * force_rotation * Time.deltaTime); 
  }
}
