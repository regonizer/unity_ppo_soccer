using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameMaster : MonoBehaviour
{
  public GameObject goal0;
  public GameObject goal1;
  public List<ControllerAgent> team0_agents;
  public List<ControllerAgent> team1_agents;
  public GameObject ball;
  private Rigidbody ball_rigidbody;
  private List<ControllerAgent> all_agents;
  private float stopwatch_time = 0f;
  private float decision_stopwatch_time = 0f;
  private Vector3 pos_ball;
  public int id = 0;

  void Start() {
    all_agents = team0_agents.Concat(team1_agents).ToList();
    ball_rigidbody = ball.GetComponent<Rigidbody>();
    pos_ball = ball.transform.position;
  }

  private void End(float team0_reward = 0f, float team1_reward = 0f, bool reset = false) {
    foreach (ControllerAgent agent in team0_agents) agent.AddReward(team0_reward);
    foreach (ControllerAgent agent in team1_agents) agent.AddReward(team1_reward);
    foreach (ControllerAgent agent in all_agents) agent.transform.position = agent.pos_agent;
    foreach (ControllerAgent agent in all_agents) agent.GetComponent<Rigidbody>().velocity = new Vector3();
    foreach (ControllerAgent agent in all_agents) agent.transform.rotation = agent.rot_agent;
    ball.transform.position = pos_ball;
    ball_rigidbody.velocity = new Vector3();
  }

  void FixedUpdate() {
    // reset ball position (this prevents the agents to clamp the ball for rewards)
    stopwatch_time += Time.fixedDeltaTime;
    if (stopwatch_time > 10f) {
      foreach (ControllerAgent agent in all_agents) agent.EndEpisode();
      stopwatch_time = 0f;
    }

    // give small reward for moving to ball position (absolute)
    foreach (ControllerAgent agent in all_agents) {
      float distance_to_ball = (agent.transform.position - ball.transform.position).magnitude;
      float disteance_to_goal = (agent.goal.transform.position - ball.transform.position).magnitude;
      agent.AddReward(0.000002f * (30 - distance_to_ball)); // reward a close position to the ball to start all agents playing around
      agent.AddReward(0.00001f * (30 - disteance_to_goal)); // the ball needs to be pushed to the goal
    }

    // register a goal and give reward
    // only positive rewards are given (the agents refused to try out things on negative rewards)
    if (ball.transform.localPosition.x < -15) End(-0.3f, 1f, true); // goal for team 1
    if (ball.transform.localPosition.x > 15) End(1f, -0.3f, true); // goal for team 0
  }
}
